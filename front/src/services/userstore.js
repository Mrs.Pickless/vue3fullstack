import axios from "axios";
import { ref } from "vue";


axios.interceptors.request.use(function (config) {
    config.headers['x-auth-token'] = localStorage.getItem('token') || "";
    return config;
});

axios.interceptors.response.use(function (response) {
    return response;
});

const user = ref(null);

function useUserStore() {
    return { user, connect, disconnect, autoConnect, inscription, getUserTodo, setUserTodo };
}

async function getUserTodo() {
    const response = await axios.get('http://localhost:3001/mytask').then(res => res).catch(err => err);
    if (response.status !== 200) {
        return null;
    }
    return response.data
}

async function setUserTodo(todo) {
    const response = await axios.post('http://localhost:3001/task', todo).then(res => res).catch(err => err);
    if (response.status !== 201) {
        return null;
    }
    return response.data
}

async function inscription(email, username, password) {
    if (email && password && username) {
        console.log("register !")
        const response = await axios.post('http://localhost:3001/register', { "email": email, "password": password, "username": username }).then(res => res).catch(err => err);
        if (response.status !== 201) {
            return null;
        }
        localStorage.setItem('token', response.headers['x-auth-token']);
        console.log(response.data)
        return user.value = { "email": response.data.email, "username": response.data.username };
    }
    else {
        return null;
    }
}

async function connect(email, password) {
    if (email && password) {
        const response = await axios.post('http://localhost:3001/login', { "email": email, "password": password }).then(res => res).catch(err => err);
        if (response.status !== 200) {
            return null;
        }
        localStorage.setItem('token', response.headers['x-auth-token']);
        return user.value = response.data.user;
    }
    else {
        return null;
    }
}
async function autoConnect() {
    if (localStorage.getItem('token')) {
        const response = await axios.post('http://localhost:3001/moncompte').then(res => res).catch(err => err);
        if (response.status !== 200) {
            localStorage.removeItem('token');
            return null;
        }
        user.value = response.data.user;
    }
}
function disconnect() {
    if (user) {
        user.value = null;
        localStorage.removeItem('token');
    }
}

export { useUserStore };
