import { createWebHistory, createRouter } from "vue-router";
import Acceuil from '@/components/Acceuil.vue';
import Login from '@/components/Login.vue';
import Register from '@/components/Register.vue';
import MyAccount from '@/components/MyAccount.vue';
import Todo from '@/components/Todo.vue'
import { useUserStore } from './services/userstore';
const { user, disconnect } = useUserStore();

const router = createRouter({
    history: createWebHistory(),
    routes: [
        {
            path: "/",
            name: "acceuil",
            component: Acceuil
        },
        {
            path: "/apropos",
            name: "apropos",
            component: Acceuil
        },
        {
            path: "/login",
            name: "login",
            component: Login
        },
        {
            path: "/register",
            name: "register",
            component: Register
        },
        {
            path: "/todo",
            name: "todo",
            component: Todo
        },
        {
            path: "/myaccount",
            name: "moncompte",
            component: MyAccount
        }
    ]
});

router.beforeEach((to, from, next) => {
    const isConnected = localStorage.getItem('token') === null ? false : true
    const routeNeedLogin = ["moncompte", "todo"]
    const routeNotNeedLogin = ["login", "register"]
    if (routeNeedLogin.includes(to.name) && !isConnected) next({ name: 'login' })
    if (routeNotNeedLogin.includes(to.name) && isConnected) next({ name: 'acceuil' })
    // if the user is not authenticated, `next` is called twice
    next()
})
export default router;